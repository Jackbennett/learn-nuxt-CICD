export const mode = 'universal';
export const router = {
    base: process.env.CI ? `/${process.env.CI_PROJECT_NAME}/` : '/'
};
export const env = {
    SITE_TYPE: process.env.CI_ENVIRONMENT_NAME || 'testing'
};
export const generate = {
    dir: 'public'
};
export const head = {
    meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ],
    htmlAttrs: {
        lang: 'en-GB'
    },
    titleTemplate: title => {return title ? `${title} | Learning Nuxt` : 'Learning Nuxt'}
}
