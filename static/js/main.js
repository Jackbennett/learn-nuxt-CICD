jQuery(function($) {
  //#main-slider
  $(function(){
    $('#main-slider .carousel').carousel({
      interval: 5000
    });
  });

  //goto section
  $(function() {
      $('a[href*=#]:not([href=#], [href=#main-slider])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top - 50
          }, 500);
          return false;
        }
      }
    });
  });
});