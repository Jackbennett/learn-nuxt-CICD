FROM node:10-alpine as build

WORKDIR /app

COPY ./package*.json /app/
RUN npm install --production
COPY . .
RUN npm run generate

FROM nginx:alpine

COPY --from=build /app/public/ /usr/share/nginx/html/

EXPOSE 80
