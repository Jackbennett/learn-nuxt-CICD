# NUXT framework for new Static Site

Testing an SPA site model for use on static file hosting.

[Colour scheme](https://coolors.co/00004d-fdfffc-c69e1b-235789-a4031f)
* `#00004d` base
* `#a4031f` - Test - [url: localhost:3000/](http://localhost:3000/)
* `#c69e1b` - Stage - [url: jackbennett.gitlab.io/learn-nuxt-CICD/](http://jackbennett.gitlab.io/learn-nuxt-CICD/)
* `#00004d` - production - Not yet in use

## Progress
* [x]  Static html can be served up without modification

    Migration can copy the entire site to `/static/`, later migrate pages into a markdown or html tree to take advantage of layout templating and webpack optimization with `/assets/`
* [ ]  Be able to directly navigate to a route, currently you get 404 for some reason. (static routes arent made for intermediary vue page routes)
* [ ]  Generate new pages from .md (markdown)
* [ ]  Generate new pages from .html (edited html source for templating)
* [ ]  Implement site template variables i.e. `{{headteacher}}`, `{{contactPhone}}`
* [ ]  New Application route for /news/
* [ ]  Route redirection when using generated static site. (otherwise will need to configure a .htaccess on host.)
* [ ]  Generate/Dont generate page content and static routes depending on publish/expire dates.
* [x]  Use Gitlab CI to generate site files
* [x]  Use Gitlab CD to serve staging/testing files, have Env variable to change site colours/logo for testing.
* [ ]  Use Gitlab Deploy target to production host upon completion

### Tests to perform
* [ ]  Linting should warn/prevent use of `/` in urls as nuxt won't rewrite static paths beginning with it
* [ ]  Test stage to check for 404 links

## File Information

File | Description
-----|-
package.json | Application information
nuxt.confg.json | Site generation information
assets/ | Files filtered through webpack
layouts/ | Site design tempaltes
pages/ | Application files
static/ | untouched files routed from `/`


## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).
